package com.example.mychat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class SignIn extends AppCompatActivity {

    private static final String TAG = "SignIn";
    public MainActivity mainActivity = new MainActivity();
    public Register register = new Register();

    // public FloatingActionButton floatingActionButton;
    public Button signUp;
    public Button signIn;
    public EditText email;
    public EditText password;
    public Toolbar toolbar;
    public TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        // floatingActionButton = findViewById(R.id.floatingActionButton);
        signUp = findViewById(R.id.button4);
        signIn = findViewById(R.id.button);
        email = findViewById(R.id.editTextTextEmailAddress2);
        password = findViewById(R.id.editTextTextPassword2);
        toolbar = (Toolbar) findViewById(R.id.registerToooBar);
        title = (TextView) findViewById(R.id.invisibleTitle);
        try {
            setSupportActionBar(toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(getSupportActionBar()).setTitle("Sign Up");
            } else {
                try {
                    getSupportActionBar().setTitle("Register");
                } catch (NullPointerException npe) {
                    title.setVisibility(View.VISIBLE);
                }
            }
        } catch (RuntimeException re) {
            Toast.makeText(SignIn.this, "Couldn't load toolbar", Toast.LENGTH_SHORT).show();
        }
/*
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Back clicked");
                openWelcome();
            }
        });

 */

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Sign Up clicked");
                openRegisterActivity();
                // mainActivity.createAccount(email.getText().toString(), password.getText().toString());
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Sign In clicked");
                mainActivity.signIn(email.getText().toString(), password.getText().toString(), new NextActivity() {
                    @Override
                    public void next() {
                        openHome();
                    }
                });
                // register.openSignIn();
            }
        });
    }

    public void openRegisterActivity() {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
        finish();
    }

    public void openWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openHome() {
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
        finish();
    }
}