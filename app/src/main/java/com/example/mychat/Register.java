package com.example.mychat;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
// import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class Register extends AppCompatActivity {

    public String Username;
    public MainActivity mainActivity = new MainActivity();
    private String TAG = "Register";

    // public FloatingActionButton floatingActionButton;
    public Button signUp;
    public Button signIn;
    public EditText email;
    public EditText password;
    public EditText username;
    public EditText phone;
    public Toolbar toolbar;
    public TextView title;

    private FirebaseAuth mAuth;

    // @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        // floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        signUp = (Button) findViewById(R.id.button);
        signIn = (Button) findViewById(R.id.button4);
        email = (EditText) findViewById(R.id.editTextTextEmailAddress);
        password = (EditText) findViewById(R.id.editTextTextPassword);
        username = (EditText) findViewById(R.id.editTextTextPersonName);
        phone = (EditText) findViewById(R.id.editTextPhone);
        toolbar = (Toolbar) findViewById(R.id.registerToooBar);
        title = (TextView) findViewById(R.id.invisibleTitle);

        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setTitle("Sign Up");
        } else {
            try {
                getSupportActionBar().setTitle("Register");
            } catch (NullPointerException npe) {
                title.setVisibility(View.VISIBLE);
            }
        }

        Username = username.getText().toString();
/*
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Back clicked");
                openWelcome();
            }
        });

 */

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Sign Up clicked");
                mainActivity.createAccount(email.getText().toString(), password.getText().toString(), mAuth, new NextActivity() {
                    @Override
                    public void next() {
                        openHome();
                    }
                });
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Sign In clicked");
                // mainActivity.signIn(email.getText().toString(), password.getText().toString());
                openSignIn();
            }
        });
    }

    public void openWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openSignIn() {
        Intent intent = new Intent(this, SignIn.class);
        startActivity(intent);
        finish();
    }

    public void openHome() {
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
        finish();
    }
}