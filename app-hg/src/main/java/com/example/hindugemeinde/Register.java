package com.example.hindugemeinde;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.auth.FirebaseAuth;

public class Register extends AppCompatActivity {

    private String TAG = "Register";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final Button register = findViewById(R.id.registering_button);
        final EditText Email = findViewById(R.id.editTextTextEmailAddress);
        final EditText Password = findViewById(R.id.editTextTextPassword);
        final EditText Phone = findViewById(R.id.editTextPhone);
        final EditText Name = findViewById(R.id.editTextTextPersonName);


        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_reg);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout_reg);
        toolBarLayout.setTitle(getTitle());

        mAuth = FirebaseAuth.getInstance();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Password.getText().toString().trim().length() < 8) {
                    Toast.makeText(Register.this, "Password must be at least 8 characters long",
                            Toast.LENGTH_SHORT).show();
                } else if (!Email.getText().toString().contains("@")) {
                    Toast.makeText(Register.this, "Password must be at least 8 characters long",
                            Toast.LENGTH_SHORT).show();
                } else {
                    MainActivity mainActivity = new MainActivity();
                    mainActivity.register(Email.getText().toString(), Password.getText().toString(),
                            Phone.getText().toString(), Name.getText().toString(), mAuth, Register.this);
                    openHomePage();
                }
            }
        });
    }

    public void openHomePage() {
        Intent intent = new Intent(this, HomePage.class);
        startActivity(intent);
    }
}